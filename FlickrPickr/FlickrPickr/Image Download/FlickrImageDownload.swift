//
//  FlickrImageDownload.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import CoreData

class FlickrImageDownload: Operation {
    
    let urlString: String
    let photoId: String
    
    var operationExecuting: Bool = true
    var operationFinished: Bool = false
    
    init(photo: Photo) {
        self.urlString = String(format: "https://farm%i.staticflickr.com/%@/%@_%@_c.jpg", photo.farm, photo.server!, photo.photoId!, photo.secret!)
        self.photoId = photo.photoId!
    }
    
    override var isExecuting: Bool {
        return operationExecuting
    }
    
    override var isFinished: Bool {
        return operationFinished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    func completeOperation() -> Void {
        willChangeValue(forKey: "isFinished")
        willChangeValue(forKey: "isExecuting")
        operationExecuting = false
        operationFinished = true
        didChangeValue(forKey: "isFinished")
        didChangeValue(forKey: "isExecuting")
    }
    
    override func main() {
        
        if self.isCancelled {
            willChangeValue(forKey: "isFinished")
            operationFinished = true
            didChangeValue(forKey: "isFinished")
            return
        }
        
        willChangeValue(forKey: "isExecuting")
        operationExecuting = true
        didChangeValue(forKey: "isExecuting")
        
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        
        do {
            let imageData = try Data(contentsOf: url)
            
            let context = CoreDataStack.shared.privateContext()
            let photo = CommonQueries.fetchPhoto(forId: self.photoId, inContext: context)
            photo.image = imageData
            
            self.canSaveContext(context: context)
            
        } catch let err as NSError {
            let windowRootVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
            let alert = UIAlertController(title: "Error", message: "There was a problem downloading photos. Please check your internet connection and try again.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            windowRootVC?.present(alert, animated: true, completion: nil)
            
            self.completeOperation()
            print(err)
        }
        
        
        
    }
    
    func canSaveContext(context: NSManagedObjectContext) -> Void {
        context.perform {
            do {
                try context.save()
            }catch let err as NSError {
                self.completeOperation()
                print(err)
            }
            
            CoreDataStack.shared.saveContext(completed: {
                self.completeOperation()
            })
        }
    }
    
}
