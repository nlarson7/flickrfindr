//
//  CommonCoreDataQueries.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import Foundation
import CoreData

class CommonQueries {
    
    class func fetchPhoto(forId photoId: String, inContext context: NSManagedObjectContext) -> Photo {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Photo")
        let predicate = NSPredicate(format: "photoId = %@", photoId)
        request.predicate = predicate
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                return results.first as! Photo
            }else {
                return Photo(context: context)
            }
        }catch let err as NSError {
            print(err)
            return Photo(context: context)
        }
        
    }
    
    class func fetchSearchTerm(forTerm term: String, inContext context: NSManagedObjectContext) -> SearchTerms {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SearchTerms")
        let predicate = NSPredicate(format: "searchText = %@", term)
        request.predicate = predicate
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                return results.first as! SearchTerms
            }else {
                return SearchTerms(context: context)
            }
        }catch let err as NSError {
            print(err)
            return SearchTerms(context: context)
        }
        
    }
    
}
