//
//  APIRequestHelpers.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import Foundation

typealias OperationCompleted = () -> ()
var operationCompleted: OperationCompleted? = { }

struct APIRequestParameters {
    var endpoint: String!
    var httpMethod: String!
    var API: API
    var additionalParameters: [String:Any]?
}

enum API: String {
    case flickrInteresting = "flickr.interestingness.getList"
    case flickrSearch = "flickr.photos.search"
}
