//
//  FlickrHandleSearch.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import Foundation

extension FlickrHandle {
    
    class func getSearchList(searchText: String) -> Void {
        
        let endpoint = self.baseUrl.appending("?").appending(String(format: "method=%@&api_key=%@&format=json&nojsoncallback=1&per_page=25&text=%@", API.flickrSearch.rawValue, self.APIKey, searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
        let params = APIRequestParameters(endpoint: endpoint, httpMethod: "GET", API: .flickrSearch, additionalParameters: ["searchText":searchText])
        
        let op = APIRequest(parameters: params)
        QueueManager.shared.downloadQueue.addOperation(op)
        
    }
    
    class func getMoreSearchList(searchText: String, currentPage: Int) -> Void {
        
        let endpoint = self.baseUrl.appending("?").appending(String(format: "method=%@&api_key=%@&format=json&nojsoncallback=1&per_page=25&page=%i&text=%@", API.flickrSearch.rawValue, self.APIKey, currentPage + 1, searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
        let params = APIRequestParameters(endpoint: endpoint, httpMethod: "GET", API: .flickrSearch, additionalParameters: ["searchText":searchText])
        
        let op = APIRequest(parameters: params)
        QueueManager.shared.downloadQueue.addOperation(op)
        
    }
    
    func handleFlickrSearch(downloadedJson json: [String: Any], parameters: APIRequestParameters, completed: OperationCompleted?) -> Void {
        
        guard let topPhotos = json["photos"] as? [String:Any] else {
            return
        }
        guard let allPhotos = topPhotos["photo"] as? [[String:Any]] else {
            return
        }
        
        guard let page = topPhotos["page"] as? Int64 else {
            return
        }
        
        guard let pages = topPhotos["pages"] as? Int64 else {
            return
        }
        
        let context = CoreDataStack.shared.privateContext()
        
        for object in allPhotos {
            if let photoId = object["id"] as? String {
                let photo = CommonQueries.fetchPhoto(forId: photoId, inContext: context)
                photo.page = page
                photo.pages = pages
                photo.farm = object["farm"] as? Int64 ?? 0
                photo.photoId = photoId
                photo.secret = object["secret"] as? String
                photo.server = object["server"] as? String
                photo.title = object["title"] as? String
                photo.downloadDate = Date()
                
                if let term = parameters.additionalParameters?["searchText"] as? String {
                    let searchTerm = CommonQueries.fetchSearchTerm(forTerm: term, inContext: context)
                    photo.searchTerm = searchTerm
                }
                
            }
        }
        
        context.perform {
            do {
                try context.save()
            }catch let err as NSError {
                print(err)
            }
            CoreDataStack.shared.saveContext(completed: {
                completed?()
            })
        }
        
    }
    
}


