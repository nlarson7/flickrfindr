//
//  WelcomeView.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit

class WelcomeView: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1.0) {
            self.containerView.layer.cornerRadius = 6.0
            self.containerView.layer.shadowColor = UIColor.black.cgColor
            self.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.containerView.layer.shadowRadius = 5.0
            self.containerView.layer.shadowOpacity = 0.5
            
            let maskPath = UIBezierPath(roundedRect: self.imageView.bounds,
                                        byRoundingCorners: [.topLeft, .topRight],
                                        cornerRadii: CGSize(width: 6.0, height: 6.0))
            
            let maskLayer = CAShapeLayer()
            maskLayer.path = maskPath.cgPath
            self.imageView.layer.mask = maskLayer
            
            self.getStartedButton.layer.cornerRadius = 6.0
            self.getStartedButton.layer.shadowColor = UIColor.black.cgColor
            self.getStartedButton.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.getStartedButton.layer.shadowRadius = 3.0
            self.getStartedButton.layer.shadowOpacity = 0.5
        }
        
    }
    
    @IBAction func getStarted() -> Void {
        
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNavigation") as! UINavigationController
        let vc = nav.viewControllers.first as! PhotoListView
        self.navigationController?.pushViewController(vc, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
    }
    
}
