//
//  PhotoDetailsView.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import CoreData

class PhotoDetailsView: UIViewController {
    
    var photo: Photo!
    
    override func viewDidLoad() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneRightButtonTapped))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        imageView.contentMode = .scaleAspectFill
        
        if let image = self.photo.image {
            imageView.image = UIImage(data: image)
        }else {
            imageView.image = #imageLiteral(resourceName: "icon-no-image")
        }
        
        self.view.addSubview(imageView)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doneRightButtonTapped)))
        
    }
    
    @objc func doneRightButtonTapped() -> Void {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}
