//
//  PhotoCell.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var captionLabel: UILabel!
    var downloadImageOperation: Operation?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.cornerRadius = 6
        imageView.layer.masksToBounds = true
    }
    
    var photo: Photo? {
        didSet {
            if let photo = photo {
                if photo.image != nil {
                    imageView.image = UIImage.init(data: photo.image!)
                }else {
                    imageView.image = nil
                }
                captionLabel.text = photo.title
                self.layer.cornerRadius = 6.0
            }
        }
    }
    
    func canDownloadImage(photo: Photo) -> Void {
        if photo.image == nil {
            let op = FlickrImageDownload(photo: photo)
            QueueManager.shared.downloadQueue.addOperation(op)
        }
    }
    
    func canStopDownloadingImage() -> Void {
        if let op = self.downloadImageOperation {
            op.cancel()
        }
    }
    
    
    
}
