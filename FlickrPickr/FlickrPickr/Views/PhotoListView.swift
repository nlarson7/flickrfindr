//
//  PhotoListView.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import CoreData

class PhotoListView: UICollectionViewController {
    
    var fetchedResultsController: NSFetchedResultsController<Photo> = NSFetchedResultsController()
    var searchTermUsed: String?
    var searchController: UISearchController!
    var resultsController = SearchResultsController()
    var fullScreenImageView: UIImageView!
    var originalImageView: UIImageView!
    let transition = PopAnimator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.searchTermUsed == nil {
            self.title = "Interesting Photos"
            self.searchController = UISearchController(searchResultsController: resultsController)
            self.searchController.dimsBackgroundDuringPresentation = false
            self.searchController.searchBar.sizeToFit()
            self.searchController.searchBar.delegate = resultsController
            self.searchController.obscuresBackgroundDuringPresentation = false
            definesPresentationContext = true
            self.navigationItem.searchController = self.searchController
            self.navigationItem.hidesSearchBarWhenScrolling = false
            self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
            resultsController.delegate = self
            self.navigationItem.hidesBackButton = true
        }else {
            self.title = self.searchTermUsed!
        }
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 10, bottom: 10, right: 10)

        if let layout = collectionView?.collectionViewLayout as? TwoColumnLayout {
            layout.delegate = self
        }
        
        configureView()
    }
    
    func configureView() {
        fetchedResultsController = listFetchedResultsController()
    }
    
}

extension PhotoListView {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        
        let photo = fetchedResultsController.object(at: indexPath)
        
        cell.photo = photo
        
        if cell.imageView.image == nil && cell.downloadImageOperation == nil {
            cell.canDownloadImage(photo: photo)
        }
        
        if fetchedResultsController.fetchedObjects!.count - indexPath.row == 3 {
            
            let currentPage = photo.page
            let totalPages = photo.pages
            
            if currentPage < totalPages {
                if self.searchTermUsed == nil {
                    FlickrHandle.getMoreInterestingList(currentPage: Int(currentPage))
                }else {
                    FlickrHandle.getMoreSearchList(searchText: self.searchTermUsed!, currentPage: Int(currentPage))
                }
            }
            
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let aCell = cell as? PhotoCell {
            aCell.canDownloadImage(photo: fetchedResultsController.object(at: indexPath))
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let aCell = cell as? PhotoCell {
            aCell.canStopDownloadingImage()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCell
        originalImageView = cell.imageView

        let photo = fetchedResultsController.object(at: indexPath)
        let vc = PhotoDetailsView()
        vc.photo = photo
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: false)
        nav.transitioningDelegate = self
        self.present(nav, animated: true, completion: nil)
    }
    
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { context in
            // If I need to do something here I can, otherwise I don't.
        }, completion: nil)
    }
    

    
}

extension PhotoListView: TwoColumnLayoutDelegate {
    func collectionView(heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        if let imgData = fetchedResultsController.object(at: indexPath).image {
            let image = UIImage.init(data: imgData)
            return image?.size.height ?? 180.0
        }
        return 180.0
    }
    
    func numberOfItemsInSection() -> Int {
        return self.collectionView?.numberOfItems(inSection: 0) ?? 0
    }
}

private extension PhotoListView {
    
    func listFetchedResultsController() -> NSFetchedResultsController<Photo> {
        let fetchedResultController = NSFetchedResultsController(fetchRequest: listFetchRequest(),
                                                                 managedObjectContext: CoreDataStack.shared.mainContext,
                                                                 sectionNameKeyPath: nil,
                                                                 cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch let error as NSError {
            fatalError("Error: \(error.localizedDescription)")
        }
        return fetchedResultController
    }
    
    func listFetchRequest() -> NSFetchRequest<Photo> {
        let fetchRequest = NSFetchRequest<Photo>(entityName: "Photo")
        fetchRequest.fetchBatchSize = 25
        if self.searchTermUsed == nil {
            let predicate = NSPredicate(format: "searchTerm = nil")
            fetchRequest.predicate = predicate
        }else {
            let searchTerm = CommonQueries.fetchSearchTerm(forTerm: self.searchTermUsed!, inContext: CoreDataStack.shared.mainContext)
            let predicate = NSPredicate(format: "searchTerm = %@", searchTerm)
            fetchRequest.predicate = predicate
        }
        
        let sortDescriptor = NSSortDescriptor(key: "downloadDate", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
}

extension PhotoListView: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if let layout = collectionView?.collectionViewLayout as? TwoColumnLayout {
            layout.invalidateLayout()
        }
        self.collectionView?.reloadData()
    }
}

extension PhotoListView: SearchResultsControllerDelegate {
    
    func searchButtonClicked(forSearchText text: String) {
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNavigation") as! UINavigationController
        let vc = nav.viewControllers.first as! PhotoListView
        vc.searchTermUsed = text
        FlickrHandle.getSearchList(searchText: text)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension PhotoListView: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        transition.originFrame = originalImageView.superview!.convert(originalImageView.frame, to: nil)
        transition.presenting = true
        
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}
