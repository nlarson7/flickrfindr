//
//  SearchResultsController.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/29/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import CoreData

protocol SearchResultsControllerDelegate {
    func searchButtonClicked(forSearchText text: String)
}

class SearchResultsController: UITableViewController {
    
    var delegate: SearchResultsControllerDelegate?
    var context: NSManagedObjectContext?
    var fetchedResultsController: NSFetchedResultsController<SearchTerms> = NSFetchedResultsController()
    
    override func viewDidLoad() {
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: String(describing: UITableViewCell.self))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.context = nil
        self.context = CoreDataStack.shared.childContext()
        fetchedResultsController = listFetchedResultsController()
    }
    
}

private extension SearchResultsController {
    
    func listFetchedResultsController() -> NSFetchedResultsController<SearchTerms> {
        let fetchedResultController = NSFetchedResultsController(fetchRequest: listFetchRequest(),
                                                                 managedObjectContext: context!,
                                                                 sectionNameKeyPath: nil,
                                                                 cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch let error as NSError {
            fatalError("Error: \(error.localizedDescription)")
        }
        return fetchedResultController
    }
    
    func listFetchRequest() -> NSFetchRequest<SearchTerms> {
        let fetchRequest = NSFetchRequest<SearchTerms>(entityName: "SearchTerms")
        fetchRequest.fetchBatchSize = 25
        
        let sortDescriptor = NSSortDescriptor(key: "searchText", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
}

extension SearchResultsController: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.reloadData()
    }
}

extension SearchResultsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UITableViewCell.self), for: indexPath)
        
        cell.textLabel?.text = self.fetchedResultsController.object(at: indexPath).searchText
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.delegate?.searchButtonClicked(forSearchText: self.fetchedResultsController.object(at: indexPath).searchText!)
    }
}

extension SearchResultsController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let char = String(searchBar.text!.remove(at: searchBar.text!.startIndex))
        if char != " " {
            searchBar.text = char.appending(searchBar.text!)
        }
        
        let searchTerm = CommonQueries.fetchSearchTerm(forTerm: searchBar.text!, inContext: self.context!)
        searchTerm.searchText = searchBar.text!
        
        self.context?.perform {
            do {
                try self.context!.save()
            }catch let err as NSError {
                print(err)
            }
            CoreDataStack.shared.saveContext(completed: {
                self.delegate?.searchButtonClicked(forSearchText: searchBar.text!)
            })
        }
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.text = " "
    }
    
}
