//
//  PopAnimator.swift
//  FlickrPickr
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit

class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.4
    var presenting = true
    var originFrame = CGRect.zero
    
    var dismissCompletion: (()->Void)?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let tView = presenting ? toView : transitionContext.view(forKey: .from)!
        
        let initialFrame = presenting ? originFrame : tView.frame
        let finalFrame = presenting ? tView.frame : originFrame
        
        let xScaleFactor = presenting ?
            (initialFrame.width + 30) / finalFrame.width :
            finalFrame.width / (initialFrame.width - 30)
        
        let yScaleFactor = presenting ?
            initialFrame.height / finalFrame.height :
            finalFrame.height / initialFrame.height
        
        let scaleTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)
        
        tView.layer.cornerRadius = 6.0
        
        
        if presenting {
            tView.transform = scaleTransform
            tView.center = CGPoint(
                x: initialFrame.midX,
                y: initialFrame.midY)
            tView.clipsToBounds = true
            tView.alpha = 0.0
        }
        
        containerView.addSubview(toView)
        containerView.bringSubview(toFront: tView)
        
        UIView.animate(withDuration: duration, animations: {
            tView.transform = self.presenting ? CGAffineTransform.identity : scaleTransform
            tView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
            if !self.presenting {
                tView.alpha = 0.0
            }else {
                tView.alpha = 1.0
            }
        }, completion: { finished in
            if !self.presenting {
                self.dismissCompletion?()
            }
            transitionContext.completeTransition(true)
        })
    }
}

